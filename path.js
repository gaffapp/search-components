module.exports = {
    spa: [  // SPAs path
        //'C:/path/to/spa1',
        //'C:/path/to/spa2',
        //'C:/path/to/spa3',
        '/media/Datos/GaffApp/FileSystem/copifolder/',
        '/media/Datos/GaffApp/FileSystem/',
        '/media/Datos/GaffApp/dtool/'
    ],
    search: { // Search for dependencies on SPAs 
        // "file-system": "^2.2.2",
        // "fs": "0.0.1-security",
        // "fs-extra": "^8.0.1",
        "file-system": "^2.2.2",
        "fsss": "0.0.1-security",
        "fs-extra": "^8.0.1",
        "rimraf": "^8.0.1",
    }
};