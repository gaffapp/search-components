var fse = require('fs-extra');
const fs = require("fs");
const chalk = require('chalk');
const path = require('./path');

const cyan = chalk.cyan;
const red = chalk.bold.red;
const green = chalk.bold.green;

var result = {};

function compare(nameSPA, dependenciesSPA, search) {
    var isFind;

    JSON.parse(JSON.stringify(dependenciesSPA), function (elDepend, vD) {
        if (elDepend.length > 0 && !result[elDepend]) {
            result[elDepend] = '';
        }

        isFind = false;
        JSON.parse(JSON.stringify(search), function (elSearch, vS) {
            if (elSearch == elDepend) {
                isFind = true;
            }

        });

        if (isFind) {
            result[elDepend] += nameSPA + ', ';
        }
    });

}

/**
 * *************************************************************************
 * Init code
 * *************************************************************************
 */
function init() {
    var nameSpa = 'SPA';
    var spa;
    var search;
    var dependencies;

    // Dependencies to compare (path.js:search)
    if (path.search && Object.keys(path.search).length > 0) {
        search = path.search;
    }

    if (path.spa && path.spa.length > 0) {
        path.spa.forEach((element) => {
            spa = require(element + 'package.json');

            if (spa.name && spa.name.length > 0) {
                nameSpa = spa.name;
            }

            if (spa.dependencies && Object.keys(spa.dependencies).length > 0) {
                dependencies = spa.dependencies;

                compare(nameSpa, dependencies, search);
            } else {
                console.log(red("Error, no exist spa.dependencies"));
            }
        });



        console.log(cyan('--------------------------------------'));
        console.log('Dependencia : SPAs');
        console.log(cyan('--------------------------------------'));
        if (result && Object.keys(result).length > 0); {
            // console.log(Object.keys(result).length);
            Object.keys(result).forEach((el) => {
                if (el.length > 0) {
                    var value = result[el].trim();
                    console.log(cyan(el) + ' : ' + green(value.substring(0, value.length-1)));
                }
            });
        }
        console.log(cyan('--------------------------------------'));

    } else {
        // Error, no exist.
        console.log(red("Error, no exist path.spa"));
    }

};

// Exect code
init();